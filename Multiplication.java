package ru.gusarov;
//выполнил студент группы Гусаров Евгений
import java.util.Scanner;

public class Multiplication {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите первый множитель: ");
        int oneFactor = scan.nextInt();
        System.out.println("Введите второй множитель: ");
        int twoFactor = scan.nextInt();
        int composition = multiplications(oneFactor,twoFactor);
        System.out.println("Произведение равно: " + composition);
    }

    private static int multiplications(int a, int b){
        int composition = 0;
        int sign = 1;
        if(a < 0) {
            a *= -1;
            sign *= -1;
        }
        if(b < 0) {
            b *= -1;
            sign *= -1;
        }
        for(int i = 1; i <= a; i++){
            composition += b;
        }

        return composition * sign;
    }
}